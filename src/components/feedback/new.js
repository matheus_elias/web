import React, { Component } from 'react';
import { Row, Input } from 'react-materialize';
import { connect } from 'react-redux';
import { createFeedback } from '../../actions';

class New extends Component {
  state = {
    evaluated_user: '',
    user_id: ''
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.createFeedback(this.state)
  }
  render() {
    return (
      <Row>
        <form className="white" onSubmit={this.handleSubmit}>
          <Input type="number" s={12} id="evaluated_user" onChange={this.handleChange} label="Empregado avaliado" />
          <Input type="number" s={12} id="user_id" onChange={this.handleChange} label="Empregado avaliador" />

          <div className="text-right">
            <button type="submit" className="blue btn grey darken-2">Cadastrar</button>
          </div>
        </form>
      </Row>
    );
  }
}

export default connect(null, { createFeedback })(New);