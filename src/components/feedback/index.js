import React, { Component } from 'react';
import { Collection, CollectionItem } from 'react-materialize';
import { connect } from 'react-redux';
import requireAuth from '../requireAuth';
import { getAllFeedback } from '../../actions';

class Feedback extends Component {
  componentDidMount() {
    this.props.getAllFeedback();
  }

  renderFeedback() {
    return this.props.feedback.map((item, index) => {
      return <CollectionItem key={index}>Empregado avaliado - {item.evaluated_user_name} |  Empregrado avaliador - {item.evaluer}</CollectionItem>
    })
  }
  
  render() {
    if (this.props.feedback) {
      return <Collection>{this.renderFeedback()}</Collection>  
    } else {
      return <div></div>
    }
  }
}

function mapStateToProps(state) {
  return {
    feedback: state.feedback.all
  }
}

export default requireAuth(connect(mapStateToProps, { getAllFeedback })(Feedback));