import React, { Component } from 'react';
import { Collection, CollectionItem } from 'react-materialize';
import { connect } from 'react-redux';
import requireAuth from '../requireAuth';
import { getEmployees } from '../../actions';

class Employees extends Component {
  componentDidMount() {
    this.props.getEmployees();
  }

  renderEmployees() {
    return this.props.employees.map((item, index) => {
      return <CollectionItem key={index}>{item.name}   -   {item.email}</CollectionItem>
    })
  }
  
  render() {
    if (this.props.employees) {
      return <Collection>{this.renderEmployees()}</Collection>  
    } else {
      return <div></div>
    }
  }
}

function mapStateToProps(state) {
  return {
    employees: state.employee.all
  }
}

export default requireAuth(connect(mapStateToProps, { getEmployees })(Employees));