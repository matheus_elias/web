import React, { Component } from 'react';
import { Row, Input } from 'react-materialize';
import { connect } from 'react-redux';
import { createEmployee } from '../../actions';

class New extends Component {
  state = {
    name: '',
    email: '',
    responsibility: '',
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.createEmployee(this.state)
  }
  render() {
    return (
      <Row>
        <form className="white" onSubmit={this.handleSubmit}>
          <Input type="text" s={12} id="name" onChange={this.handleChange} label="Nome" />
          <Input type="text" s={12} id="email" onChange={this.handleChange} label="E-mail" />
          <Input type="text" s={12} id="responsibility" onChange={this.handleChange} label="Cargo" />
          <Input type="password" s={12} id="password" onChange={this.handleChange} label="Senha" />
          <Input type="password" s={12} id="password_confirmation" onChange={this.handleChange} label="Confirmação de senha" />


          <div className="text-right">
            <button type="submit" className="blue btn grey darken-2">Cadastrar</button>
          </div>
        </form>
      </Row>
    );
  }
}

export default connect(null, { createEmployee })(New);