import React, { Component } from 'react';
import { Row, Col, Card, Input } from 'react-materialize';
import { connect } from 'react-redux';
import { signin } from '../../actions';

class Signin extends Component {
  state = {
    email: '',
    password: ''
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    this.props.signin(this.state, this.props.history)
  }

  render() {
    return (
      <Row>
        <Col s={6} offset="2">
          <Card>
            <form className="white" onSubmit={this.handleSubmit}>
              <Input s={12} label="E-mail" id="email" onChange={this.handleChange} />
              <Input type="password" label="Senha" s={12} id="password" onChange={this.handleChange} />

              <div className="text-right">
                <button type="submit" className="blue btn grey darken-2">Sign In</button>
              </div>
            </form>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default connect(null, { signin })(Signin);