import axios from 'axios';
import { AUTH_USER } from './types';

export const signin = (formProps, history) => {
  return async(dispatch) => {
    try {
      const response = await axios.post('http://localhost:5000/api/v1/user_token', { auth: { 
        email: formProps.email, 
        password: formProps.password 
      }});

      dispatch({
        type: AUTH_USER,
        payload: response.data.jwt
      });

      localStorage.setItem('token', response.data.jwt);
      history.push('/')
    } catch(error) {
      console.log(error);
    }
  }
}

export const signout = () => {
  localStorage.removeItem('token');

  return {
    type: AUTH_USER,
    payload: '',
  };
};