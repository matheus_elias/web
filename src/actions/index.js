import axios from 'axios';
axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('token')}`;

export * from './auth';
export * from './feedback';
export * from './employee';