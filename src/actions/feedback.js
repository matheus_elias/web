import axios from 'axios';
import { GET_ALL_FEEDBACK, CREATE_FEEDBACK} from './types';

export const getAllFeedback = () => {
  return async(dispatch) => {
    try {
      const response = await axios.get('http://localhost:5000/api/v1/feedback');
      
      dispatch({
        type: GET_ALL_FEEDBACK,
        payload: response.data
      });

    } catch(error) {
      console.log(error);
    }
  }
}

export const createFeedback = (formProps) => {
  return async(dispatch) => {
    try {
      const response = await axios.post('http://localhost:5000/api/v1/feedback', formProps);
      dispatch({
        type: CREATE_FEEDBACK,
        payload: response.data
      });

    } catch(error) {
      console.log(error);
    }
  }
}