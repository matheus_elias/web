import axios from 'axios';
import { GET_EMPLOYEES, CREATE_EMPLOYEE, GET_CURRENT } from './types';

export const getCurrent = () => {
  return async(dispatch) => {
    try {
      const response = await axios.get('http://localhost:5000/api/v1/users/current');
      dispatch({
        type: GET_CURRENT,
        payload: response.data
      });

    } catch(error) {
      console.log(error);
    }
  }
}

export const getEmployees = () => {
  return async(dispatch) => {
    try {
      const response = await axios.get('http://localhost:5000/api/v1/users');
      
      dispatch({
        type: GET_EMPLOYEES,
        payload: response.data
      });

    } catch(error) {
      console.log(error);
    }
  }
}

export const createEmployee = (formProps) => {
  return async(dispatch) => {
    try {
      const response = await axios.post('http://localhost:5000/api/v1/users', { user: formProps });
      dispatch({
        type: CREATE_EMPLOYEE,
        payload: response.data
      });

    } catch(error) {
      console.log(error);
    }
  }
}