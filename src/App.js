import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Navbar, NavItem } from 'react-materialize';
import { connect } from 'react-redux';
import { signout, getCurrent } from './actions';
import Feedback from './components/feedback';
import FeedbackNew from './components/feedback/new';
import Employees from './components/employees';
import EmployeeNew from './components/employees/new';
import Signin from './components/signin';
import Index from './components';

class App extends Component {
  componentDidMount() {
    if (!this.props.currentUser) {
      this.props.getCurrent()
    }
  }

  renderSigninSignout() {
    if (this.props.auth) {
      return <NavItem onClick={() => this.props.signout()}>Sair</NavItem>
    } else {
      return <NavItem href='/signin'>Entrar</NavItem>
    }
  }

  renderNavBar() {
    if (this.props.currentUser) {
      if (this.props.currentUser.responsibility === 'admin') {
        return (
          <Navbar brand='My-Feedback' right>
            <NavItem href='/feedback'>Feedback</NavItem>
            <NavItem href='/employees'>Empregados</NavItem>
            {this.renderSigninSignout()}
          </Navbar>
        );
      } else {
        return (
          <Navbar brand='My-Feedback' right>
            {this.renderSigninSignout()}
          </Navbar>
        );
      }
    } else {
      return (
        <Navbar brand='My-Feedback' right>
          {this.renderSigninSignout()}
        </Navbar>
      );
    }
  }
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          {this.renderNavBar()}
          <Switch>
            <Route exact path="/" component={Index} />
            <Route exact path="/feedback" component={Feedback} />
            <Route exact path="/signin" component={Signin} />
            <Route exact path="/employees" component={Employees} />
            <Route exact path="/feedback/new" component={FeedbackNew}/>
            <Route exact path="/employees/new" component={EmployeeNew} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.employee.current,
    auth: state.auth.authenticated
  }
}

export default connect(mapStateToProps, { signout, getCurrent })(App);