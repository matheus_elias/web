import { GET_EMPLOYEES, GET_CURRENT } from '../actions/types';

const INITIAL_STATE = {
  user: '',
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case GET_EMPLOYEES:
      return { ...state, all: action.payload };
    case GET_CURRENT:
      return { ...state, current: action.payload };
    default:
      return state;
  }
}