import { GET_ALL_FEEDBACK } from '../actions/types';

const INITIAL_STATE = {
  feedback: '',
};

export default function(state = INITIAL_STATE, action) {
  switch(action.type) {
    case GET_ALL_FEEDBACK:
      return { ...state, all: action.payload };
    default:
      return state;
  }
}