import { combineReducers } from 'redux';
import auth from './auth';
import feedback from './feedback';
import employee from './employee';

export default combineReducers({
  auth,
  feedback,
  employee,
});